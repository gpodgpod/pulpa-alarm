#include <MKL25Z4.h>
#include <MemMapPtr_KL25Z4.h>
#include "keypad.h"
#include "mma8451.h"
#include "display.h"
#include "led.h"
#include "my_energy.h"
#include "my_time.h"


// forward declaration: keypad update state function (to be called on interrupt)
// in keypad.c
void keypad_update(void);


void PORTA_IRQHandler(void){
	// Disable interrupts
	NVIC_DisableIRQ(PORTA_IRQn);

	// Clear pending interrupts
	NVIC_ClearPendingIRQ(PORTA_IRQn);
	
	// Accelerometer 
	if(PORTA->ISFR & (1<<14)){
		// Set data ready
		mma_data_ready = 1;

		mma_read_full_xyz();
		mma_convert_xyz_to_roll_pitch();
		
	// Keypad
	}else if((PORTA->ISFR & (1<<COL1_POS)) 
		|| (PORTA->ISFR & (1<<COL2_POS)) 
		|| (PORTA->ISFR & (1<<COL3_POS)) 
		|| (PORTA->ISFR & (1<<COL4_POS))){
			keypad_update();
	}
	
	// Clear status flags
	PORTA->ISFR = 0xffffffff;

	// Clear pending interrupts
	NVIC_ClearPendingIRQ(PORTA_IRQn);

	// Enable interrupts
	NVIC_EnableIRQ(PORTA_IRQn);
}


void LPTimer_IRQHandler(void){
	// Update application's data
	my_energy_probe_current_voltage();
	my_time_increment_current_time();

	// Clear
	//NVIC_ClearPendingIRQ(LPTimer_IRQn);
	LPTMR0->CSR |= LPTMR_CSR_TCF_MASK;
	LPTMR0->CSR |= LPTMR_CSR_TEN_MASK;
}

// TODO
/////////////////////////////
