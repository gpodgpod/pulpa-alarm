#include "my_view.h"
#include <MKL25Z4.h>
#include "delay.h"
#include "speaker.h"
#include "note.h"
#include "adc.h"
#include "mma8451.h"
#include "display.h"
#include "i2c_api.h"
#include "lptmr.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "my_energy.h"
#include "my_time.h"
#include "my_utils.h"
#include "fonts.h"
#include "my_7seg.h"
#include "my_model.h"
#include "my_images.h"
#include <math.h>

MY_SCREEN_CLOCK_FORMAT current_format = DIGITAL_24;

void my_view_set_format(MY_SCREEN_CLOCK_FORMAT format) {
    current_format = format;
}

void my_view_display_config_alarm(uint32_t selected_alarm, enum keypad_key focused_item) {
    MY_MODEL_ALARM * alarm = my_model_get_alarm(selected_alarm);

    display_clear_buffer();
    my_utils_reset_cursor();
    static char alarm_display[30];

    if(focused_item == KEYPAD_KEY_1) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    sprintf(alarm_display, "1) Hour       %lu\n", alarm->hour);
    display_draw_string(alarm_display);

    if(focused_item == KEYPAD_KEY_2) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    sprintf(alarm_display, "2) Minute     %lu\n", alarm->minute);
    display_draw_string(alarm_display);

    if(focused_item == KEYPAD_KEY_3) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    if(alarm->on) {
        sprintf(alarm_display, "3) Status     on\n");
    } else {
        sprintf(alarm_display, "3) Status     off\n");
    }
    display_draw_string(alarm_display);

    if(focused_item == KEYPAD_KEY_4) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    sprintf(alarm_display, "4) Delete\n");
    display_draw_string(alarm_display);
    display_show_buffer();
}

void my_view_display_config_alarm_list_menu(uint32_t focused_alarm) {
    assert(focused_alarm < my_model_get_alarm_count());
    display_clear_buffer();
    my_utils_reset_cursor();
    display_draw_string("  Nr  Status   Time\n");
    static char alarm_display[30];
    for(uint32_t i = 0; i < my_model_get_alarm_count(); i++) {
        MY_MODEL_ALARM * alarm_i = my_model_get_alarm(i);
        // Depends on whether the alarm is on or off
        char * alarm_status_str;
        if (alarm_i->on) {
            alarm_status_str = "ON \0";
        } else {
            alarm_status_str = "OFF\0";
        }

        // Depends on whether the alarm is selected or not 
        char alarm_number_str[5];
        if (focused_alarm == i) {
            sprintf(alarm_number_str, "> %lu", i);
        } else {
            sprintf(alarm_number_str, "  %lu", i);
        }

        sprintf(alarm_display, "%s   %s      %02lu:%02lu\n", alarm_number_str, alarm_status_str, alarm_i->hour, alarm_i->minute);
        display_draw_string(alarm_display);
    }
    display_show_buffer();
}

void my_view_display_config_format_menu(enum keypad_key focused_item) {
    my_utils_reset_cursor();
    display_clear_buffer();
    if(focused_item == KEYPAD_KEY_1) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    display_draw_string("1) 24 format\n");
    if(focused_item == KEYPAD_KEY_2) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    display_draw_string("2) 12 Format\n");
    if(focused_item == KEYPAD_KEY_3) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    display_draw_string("3) Analog format\n");
    display_show_buffer();
}

void my_view_display_config_menu(enum keypad_key focused_item) {
    my_utils_reset_cursor();
    display_clear_buffer();
    if (focused_item == KEYPAD_KEY_1) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    display_draw_string("1) Alarm lists\n");
    if (focused_item == KEYPAD_KEY_2) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    display_draw_string("2) Change display\n");
    if (focused_item == KEYPAD_KEY_STAR) {
        display_draw_string("> ");
    } else {
        display_draw_string("  ");
    }
    display_show_buffer();
}

void my_view_render_battery() {
    static char battery[5];
    if (my_energy_is_battery_powered()) {
        display_set_cursor(0, 15);
        sprintf(battery, "%.2fV", my_energy_get_current_voltage());
        display_draw_string(battery);
    } else {
        display_set_cursor(0, 12);
        display_draw_string("Charging");
    }
}

void my_view_analog_time_screen() {
    my_time_day_time time24 = my_time_get_current_time_12h();
    my_utils_draw_circle(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2 + 5, 22);
    my_utils_draw_clock_line(time24.minute, 60.f, DISPLAY_HEIGHT / 4 - 5);
    my_utils_draw_clock_line(time24.seconds, 60.f, DISPLAY_HEIGHT / 2 - 10);
}

void my_view_digital_time24_screen() {
    my_time_day_time time24 = my_time_get_current_time_24h();
    static char time[5];
    sprintf(time, "%02d%02d", time24.minute, time24.seconds);
    my_7seg_set_cursor(0, 20);
    my_7seg_draw_string(time);
}

void my_view_digital_time12_screen() {
    my_time_day_time time12 = my_time_get_current_time_12h();
    static char time[5];
    sprintf(time, "%02d%02d", time12.minute, time12.seconds);
    my_7seg_set_cursor(0, 20);
    my_7seg_draw_string(time);
}

void my_view_display_main() {
    display_clear_buffer();
    switch(current_format) {
        case DIGITAL_24:
            my_view_digital_time24_screen();
        break;
        case DIGITAL_12:
            my_view_digital_time12_screen();
        break;

        case DIGITAL_ANALOG:
            my_view_analog_time_screen();
        break;
    }
    my_view_render_battery();
    display_show_buffer();
}

void my_view_display_boot() {
  // Show boot logo
  display_clear_buffer();
  display_set_display_on();
  display_draw_image(display_splash_screen, 0, 0,
  DISPLAY_WIDTH, DISPLAY_HEIGHT, DISPLAY_DRAW_IMAGE_NORMAL);
  display_show_buffer();
}

void my_view_display_startup() {
  display_clear_buffer();
  display_draw_image(my_images_wecker, 0, 0,
  DISPLAY_WIDTH, DISPLAY_HEIGHT, DISPLAY_DRAW_IMAGE_NORMAL);
  display_show_buffer();
}