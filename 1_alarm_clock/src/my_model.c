#include "my_model.h"
#include <assert.h>
#include <string.h>

MY_MODEL_ALARM MY_MODEL_ALARM_BUFFER[MY_MODEL_ALARM_CAPACITY];

uint32_t my_model_alarm_count = 0;

void my_model_init() {
    my_model_alarm_count = 1;
    MY_MODEL_ALARM_BUFFER[0].hour = 1;
    MY_MODEL_ALARM_BUFFER[0].minute = 2;
    MY_MODEL_ALARM_BUFFER[0].on = 0;
}

void my_model_add_alarm(MY_MODEL_ALARM alarm) {
    assert(my_model_alarm_count < MY_MODEL_ALARM_CAPACITY);
    MY_MODEL_ALARM_BUFFER[my_model_alarm_count++] = alarm;
}

void my_model_delete_alarm(uint32_t alarm_pos) {
    if(alarm_pos < my_model_alarm_count) {
        void * destination = MY_MODEL_ALARM_BUFFER + alarm_pos;
        void * source = MY_MODEL_ALARM_BUFFER + alarm_pos + 1;
        uint32_t num = (my_model_alarm_count - alarm_pos - 1) * sizeof(MY_MODEL_ALARM);
        memmove(destination, source, num);
        my_model_alarm_count--;
    }
}

MY_MODEL_ALARM * my_model_get_alarm(uint32_t alarm_pos) {
    return &MY_MODEL_ALARM_BUFFER[alarm_pos];
}


uint32_t my_model_get_alarm_count() {
    return my_model_alarm_count;
}

void my_model_increase_hour(uint32_t alarm_pos) {
    MY_MODEL_ALARM_BUFFER[alarm_pos].hour++;
    MY_MODEL_ALARM_BUFFER[alarm_pos].hour %= 24;
}

void my_model_decrease_hour(uint32_t alarm_pos) {
    MY_MODEL_ALARM_BUFFER[alarm_pos].hour--;
    MY_MODEL_ALARM_BUFFER[alarm_pos].hour %= 24;
}

void my_model_increase_minute(uint32_t alarm_pos) {
    MY_MODEL_ALARM_BUFFER[alarm_pos].minute++;
    MY_MODEL_ALARM_BUFFER[alarm_pos].minute %= 60;
}

void my_model_decrease_minute(uint32_t alarm_pos) {
    MY_MODEL_ALARM_BUFFER[alarm_pos].minute--;
    MY_MODEL_ALARM_BUFFER[alarm_pos].minute %= 60;
}


void my_model_toggle_status(uint32_t alarm_pos) {
    MY_MODEL_ALARM_BUFFER[alarm_pos].on = !MY_MODEL_ALARM_BUFFER[alarm_pos].on;
}