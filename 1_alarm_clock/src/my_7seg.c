#include "display.h"
#include "fonts.h"
#include "my_7seg.h"

unsigned int cursor_x = 0;
unsigned int cursor_y = 0;
const int symbol_width = 29;
const int symbol_height = 48;


void my_7seg_set_cursor(unsigned int new_cursor_x, unsigned int new_cursor_y) {
    cursor_x = new_cursor_x;
    cursor_y = new_cursor_y;
}

void my_7seg_reset_cursor() {
    cursor_x = 0;
    cursor_y = 0;
}

void my_7seg_next_line() {
	cursor_x = 0;
	cursor_y += symbol_height + 1;
	if (cursor_y > DISPLAY_HEIGHT - symbol_height) {
		cursor_x = 0;
		cursor_y = 0;
	}
}

void my_7seg_next_col() {
    cursor_x += symbol_width + 1;
    if (cursor_x > DISPLAY_WIDTH - 5) {
        my_7seg_next_line();
    }
}

void my_7seg_draw_char(const char c) {
    const int symbol_size = 174;
    
    const unsigned char* symbol  = 0;

	switch (c) {
		case '\0': return;
		case '\n': my_7seg_next_line(); return;
        case '0': symbol = &font_seven_segment_numbers2[0 * symbol_size]; break;
        case '1': symbol = &font_seven_segment_numbers2[1 * symbol_size]; break;
        case '2': symbol = &font_seven_segment_numbers2[2 * symbol_size]; break;
        case '3': symbol = &font_seven_segment_numbers2[3 * symbol_size]; break;
        case '4': symbol = &font_seven_segment_numbers2[4 * symbol_size]; break;
        case '5': symbol = &font_seven_segment_numbers2[5 * symbol_size]; break;
        case '6': symbol = &font_seven_segment_numbers2[6 * symbol_size]; break;
        case '7': symbol = &font_seven_segment_numbers2[7 * symbol_size]; break;
        case '8': symbol = &font_seven_segment_numbers2[8 * symbol_size]; break;
        case '9': symbol = &font_seven_segment_numbers2[9 * symbol_size]; break;
        case ' ': my_7seg_next_col(); return;
		default: break;
	}

    display_draw_image(symbol, cursor_x, cursor_y, symbol_width, symbol_height, DISPLAY_DRAW_IMAGE_NORMAL);
    my_7seg_next_col();
}

void my_7seg_draw_string(const char *str) {
	for (int i = 0; str[i] != '\0'; i++) {
		my_7seg_draw_char(str[i]);
	}
}