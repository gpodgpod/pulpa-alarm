#include "my_utils.h"
#include "MKL25Z4.h"
#include "display.h"
#include "keypad.h"
#include <assert.h>
#include <math.h>

void my_utils_reset_cursor() {
    display_set_cursor(0, 0);
}

char my_utils_keypad_read_keypress_to_char() {
	enum keypad_key key = keypad_read_keypress();
	switch (key) {
	case KEYPAD_KEY_0:
	case KEYPAD_KEY_1:
	case KEYPAD_KEY_2:
	case KEYPAD_KEY_3:
	case KEYPAD_KEY_4:
	case KEYPAD_KEY_5:
	case KEYPAD_KEY_6:
	case KEYPAD_KEY_7:
	case KEYPAD_KEY_8:
	case KEYPAD_KEY_9:
		return '0' + key;
		break;
	case KEYPAD_KEY_A: return 'A'; break;
	case KEYPAD_KEY_B: return 'B'; break;
	case KEYPAD_KEY_C: return 'C'; break;
	case KEYPAD_KEY_D: return 'D'; break;
	case KEYPAD_KEY_STAR: return '*'; break;
	case KEYPAD_KEY_HASH: return '#'; break;
	default: return '\0';
	}
	assert(0);
}

void my_utils_draw_circle(uint32_t c_x, uint32_t c_y, uint32_t radius) {
	for(uint32_t i = 0; i < 360; i++) {
		float theta = (float)i / 360.0f;
		uint32_t x = c_x + radius * sin(theta * 2 * M_PI);
		uint32_t y = c_y + radius * cos(theta * 2 * M_PI);
		display_draw_pixel(x, y);
	}
}

float my_utils_reminder(float a, float b) {
    return a - (float)((int)(a / b)) * b;
}

void my_utils_draw_clock_line(float value, float max_value, float radius) {
    int center_x = DISPLAY_WIDTH / 2;
    int center_y = DISPLAY_HEIGHT /2 + 5;

    float angle_offset = M_PI_2;
    value = my_utils_reminder(value, max_value);
    float max_angle = 2.f * M_PI;
    float angle = my_utils_reminder(angle_offset + (value / max_value * max_angle), max_angle);
    float target_x = center_x - cos(angle) * radius;
    float target_y = center_y - sin(angle) * radius;

    display_draw_line(center_x, center_y, (int)target_x, (int)target_y);
}