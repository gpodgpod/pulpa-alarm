#include "my_energy.h"
#include "adc.h"

float current = 0.0;


void my_energy_probe_current_voltage() {
    current = adc_measure_vref();
}

float my_energy_get_current_voltage() {
    return current;
}

int my_energy_is_battery_powered() {
    // Assume to be battery powered if the voltage is below 2.7
    return my_energy_get_current_voltage() <= 2.7;
}