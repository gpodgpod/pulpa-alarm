#include <MKL25Z4.h>
#include <stdlib.h>
#include <stdio.h>
#include "delay.h"
#include "speaker.h"
#include "note.h"
#include "adc.h"
#include "mma8451.h"
#include "display.h"
#include "i2c_api.h"
#include "lptmr.h"
#include "keypad.h"
#include "fonts.h"
#include "my_energy.h"
#include "my_time.h"
#include "my_utils.h"
#include "my_control.h"
#include "my_view.h"
#include "my_model.h"

void my_control_init_state_machine() {
  // Enable interrupt
  __enable_irq();

  // Initialize keypad
  keypad_init();

  // Init battery
  adc_init();

  // Init timer
  my_time_init(13*60*60);//0);

  // Init display
  i2c_init(DISPLAY_I2C_BUS);
  display_enable_pins();
  display_init();

  // Init view
  my_view_set_format(DIGITAL_ANALOG);//24);

  // Init model
  my_model_init();
}

void my_control_config_alarm(uint32_t selected_alarm) {
  enum keypad_key focused_item = KEYPAD_KEY_1;
  while (1) {
    my_view_display_config_alarm(selected_alarm, focused_item);
    __WFI();
    enum keypad_key pressed = keypad_read_keypress();
    switch (pressed) {
      case KEYPAD_KEY_1:;
      case KEYPAD_KEY_2:;
      case KEYPAD_KEY_3:;
      case KEYPAD_KEY_4:;
        focused_item = pressed;
        break;
      case KEYPAD_KEY_STAR:
        return;
      case KEYPAD_KEY_A:
        if(focused_item > KEYPAD_KEY_1) {
          focused_item--;
        }
        break;
      case KEYPAD_KEY_B:
        if(focused_item < KEYPAD_KEY_4) {
          focused_item++;
        }
        break;
      case KEYPAD_KEY_C:
        if(focused_item == KEYPAD_KEY_1) {
          my_model_increase_hour(selected_alarm);
        }
        if(focused_item == KEYPAD_KEY_2) {
          my_model_increase_minute(selected_alarm);
        }
        if(focused_item == KEYPAD_KEY_3) {
          my_model_toggle_status(selected_alarm);
        }
        if(focused_item == KEYPAD_KEY_4) {
          my_model_delete_alarm(selected_alarm);
        }
        break;
      case KEYPAD_KEY_D:
        if(focused_item == KEYPAD_KEY_1) {
          my_model_decrease_hour(selected_alarm);
        }
        if(focused_item == KEYPAD_KEY_2) {
          my_model_decrease_minute(selected_alarm);
        }
        if(focused_item == KEYPAD_KEY_3) {
          my_model_toggle_status(selected_alarm);
        }
        if(focused_item == KEYPAD_KEY_4) {
          my_model_delete_alarm(selected_alarm);
        }
        break;
      case KEYPAD_KEY_HASH:
        if(focused_item == KEYPAD_KEY_4) {
          my_model_delete_alarm(selected_alarm);
        }
        return;
      default:
        break;
    }
  }
}

void my_control_config_alarm_list_menu() {
  uint32_t focused_alarm = 0;
  while (1) {
    my_view_display_config_alarm_list_menu(focused_alarm);
    __WFI();
    enum keypad_key pressed = keypad_read_keypress();
    switch (pressed) {
      case KEYPAD_KEY_0:;
      case KEYPAD_KEY_1:;
      case KEYPAD_KEY_2:;
      case KEYPAD_KEY_3:;
      case KEYPAD_KEY_4:;
      case KEYPAD_KEY_5:;
      case KEYPAD_KEY_6:;
      case KEYPAD_KEY_7:;
        if(pressed < my_model_get_alarm_count())  {
          focused_alarm = pressed;
          my_control_config_alarm(pressed);
        }
        break;
      case KEYPAD_KEY_A:
        if(focused_alarm > 0) {
          focused_alarm--;
        }
        break;
      case KEYPAD_KEY_B:
        if(focused_alarm < my_model_get_alarm_count() - 1) {
          focused_alarm++;
        }
        break;
      case KEYPAD_KEY_HASH:
        my_control_config_alarm(focused_alarm);
        break;
      case KEYPAD_KEY_STAR: return;
      default: break;
    }
  }
}

void my_control_config_format_menu() {
  enum keypad_key focused_item = KEYPAD_KEY_1;
  while (1) {
    my_view_display_config_format_menu(focused_item);
      __WFI();
    enum keypad_key pressed = keypad_read_keypress();
    switch (pressed) {
    case KEYPAD_KEY_A:
      if(focused_item > KEYPAD_KEY_1) {
        focused_item--;
      }
      break;
    case KEYPAD_KEY_B:
      if(focused_item < KEYPAD_KEY_3) {
        focused_item++;
      }
      break;
    case KEYPAD_KEY_1: 
      focused_item = pressed;
      my_view_set_format(DIGITAL_24); 
      return;
    case KEYPAD_KEY_2: 
      focused_item = pressed;
      my_view_set_format(DIGITAL_12); 
      return;
    case KEYPAD_KEY_3: 
      focused_item = pressed;
      my_view_set_format(DIGITAL_ANALOG); 
      return;
    case KEYPAD_KEY_STAR: return;
    case KEYPAD_KEY_HASH: 
      if (focused_item == KEYPAD_KEY_1) {
        my_view_set_format(DIGITAL_24); 
      } else if (focused_item == KEYPAD_KEY_2) {
        my_view_set_format(DIGITAL_12); 
      } else if (focused_item == KEYPAD_KEY_3) {
        my_view_set_format(DIGITAL_ANALOG); 
      }
      return;
    default: break;
    }
  }
}

void my_control_config_menu() {
  enum keypad_key focused_item = KEYPAD_KEY_1;
  while (1) {
    my_view_display_config_menu(focused_item);
      __WFI();
    enum keypad_key pressed = keypad_read_keypress();
    switch (pressed) {
      case KEYPAD_KEY_A:
        if(focused_item == KEYPAD_KEY_2) {
          focused_item--;
        }
        break;
      case KEYPAD_KEY_B:
        if(focused_item == KEYPAD_KEY_1) {
          focused_item++;
        }
        break;
      case KEYPAD_KEY_1: 
        focused_item = pressed;
        my_control_config_alarm_list_menu();
        break;
      case KEYPAD_KEY_2: 
        focused_item = pressed;
        my_control_config_format_menu();
        break;
      case KEYPAD_KEY_STAR: return;
      case KEYPAD_KEY_HASH: 
        if(focused_item == KEYPAD_KEY_1) {
          my_control_config_alarm_list_menu();
        } else if(focused_item == KEYPAD_KEY_2) {
          my_control_config_format_menu();
        }
      break;
      default: break;
    }
  }
}

void my_control_main() {
  while (1) {
    my_view_display_main();
      __WFI();
    enum keypad_key pressed = keypad_read_keypress();
    if (pressed == KEYPAD_KEY_HASH) {
      my_control_config_menu();
    }
   }
}

void my_control_startup() {
  my_view_display_startup();
  my_time_delay_second(1);
  my_control_main();
}

void my_control_boot() {
  my_view_display_boot();
  my_time_delay_second(1);
  my_control_startup();
}

void my_control_start_state_machine(){
  //my_control_config_alarm_list_menu();
  my_control_main();
  my_control_boot();
}