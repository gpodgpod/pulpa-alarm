#include "my_control.h"

/*----------------------------------------------------------------------------
  MAIN function
 *----------------------------------------------------------------------------*/
int main (void) {
  my_control_init_state_machine();
  my_control_start_state_machine();
}
