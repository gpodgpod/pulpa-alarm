#include "lptmr.h"
#include <stdio.h>
#include "my_time.h"

volatile uint32_t current_time = 0;
uint32_t time_delta = 1;

void my_time_init(uint32_t start_time) {
    current_time = start_time;
	SIM->SCGC5 |= SIM_SCGC5_LPTMR_MASK;

	SIM->SOPT1 = SIM_SOPT1_OSC32KSEL_MASK;


	LPTMR0->PSR = LPTMR_PSR_PRESCALE(0) | LPTMR_PSR_PBYP_MASK | LPTMR_PSR_PCS(0b01);

	LPTMR0->CMR = LPTMR_CMR_COMPARE(time_delta * 1000); // Configure this for precise time.
	
	LPTMR0->CSR = LPTMR_CSR_TPS(0) | LPTMR_CSR_TCF_MASK; // Timer enable
	LPTMR0->CSR |= LPTMR_CSR_TIE_MASK; // Timer enable
	LPTMR0->CSR |= LPTMR_CSR_TEN_MASK;
	
	NVIC_SetPriority(LPTimer_IRQn, 128); // 0, 64, 128 or 192
	NVIC_ClearPendingIRQ(LPTimer_IRQn);
	NVIC_EnableIRQ(LPTimer_IRQn);
}

uint32_t my_time_get_current_time() {
    return current_time;
}

void my_time_increment_current_time() {
	current_time += time_delta;
}

void my_time_delay_second(uint32_t seconds) {
	uint32_t start = my_time_get_current_time();
	uint32_t end = start + seconds;
	while(my_time_get_current_time() <= end) {
		__WFI();
	}
}


my_time_day_time my_time_get_current_time_24h() {
    uint32_t secs = my_time_get_current_time();
    const int SECS_IN_MIN = 60;
    const int MIN_IN_HOUR = 60;
    const int HOUR_IN_DAY = 24;
    const int SECS_IN_DAY = SECS_IN_MIN * MIN_IN_HOUR * HOUR_IN_DAY;
    const int SECS_IN_HOUR = SECS_IN_MIN * MIN_IN_HOUR;

    // for this to work secs has to be set to the time of the day
    // e.g. my_time_get_current_time() = offset(seconds of configured time) + elapsed seconds
    int secs_of_day = secs % SECS_IN_DAY;
    int hour = secs_of_day / SECS_IN_HOUR;
    int minute = (secs_of_day % SECS_IN_HOUR) / SECS_IN_MIN;
    int seconds = (secs_of_day % SECS_IN_HOUR) % SECS_IN_MIN;
	
	my_time_day_time ret;
	ret.seconds = seconds;
	ret.minute = minute;
	ret.hour = hour;
	ret.ampm = hour / 12;
	return ret;
}

my_time_day_time my_time_get_current_time_12h() {
	my_time_day_time ret = my_time_get_current_time_24h();
	ret.hour = ret.hour % 12;
	return ret;
}