CC := arm-none-eabi-gcc
C_STD := -std=gnu11
# C_OPTS can be set via the command line to add gcc options

# default MODE is debug.
# For release mode, set MODE on command line: make MODE=release all ...
MODE:=debug

# compiler flags for debug/release versions
ifeq ($(MODE),release)
	MODE:=release
	OPTIMIZE_FLAG := -Os
	DEBUG_OPTIONS := 
	USE_SEMIHOSTING :=
	TRACE_SEMIHOSTING :=
	# for openocd "run" target
	OPENOCD_RUN_OPTIONS := -c "arm semihosting enable"
else ifeq ($(MODE),debug)
	MODE:=debug
	OPTIMIZE_FLAG := -Og
	DEBUG_OPTIONS := -g3 -DDEBUG
	USE_SEMIHOSTING := -DOS_USE_SEMIHOSTING
	TRACE_SEMIHOSTING := -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG
	# for openocd "run" target
	OPENOCD_RUN_OPTIONS := -c "arm semihosting enable"
else
  $(error ERROR: MODE=$(MODE). Must be set to debug or release)
endif

BUILD_DIR := build/$(MODE)

CFLAGS := -mcpu=cortex-m0plus -mthumb $(OPTIMIZE_FLAG) -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections $(C_STD) $(DEBUG_OPTIONS) $(USE_SEMIHOSTING) $(TRACE_SEMIHOSTING) $(C_OPTS)
CFLAGS += -Wall -Werror=implicit-function-declaration -Werror

# The following warning(s) can occur in the initial exercise code (eg. day2_4, day5)
# or e.g. when commenting out some debug code.
# They should not be ignored for the final submission of the exercise
# and can be safely commented out in this makefile by the students.
CFLAGS += -Wno-error=unused-variable -Wno-error=unused-function -Wno-error=unused-const-variable

# student exercise folders
EX_INCLUDE := include/exercise
PULPA_ROOT := .
PULPA_INCLUDE := $(PULPA_ROOT)/include/pulpa_libs
PULPA_LIB_DIR := $(PULPA_ROOT)/lib/$(MODE)
SYS_INCLUDE := ./include/system
SYS_LIB_DIR := ./lib/$(MODE)
LINK_SCRIPT_DIR := ./ldscripts


INC_DIRS := -I$(EX_INCLUDE) -I$(SYS_INCLUDE) -I$(SYS_INCLUDE)/cmsis -I$(SYS_INCLUDE)/kl25-sc -I$(PULPA_INCLUDE)

# linker: libraries and options
LDFLAGS := -T $(LINK_SCRIPT_DIR)/mem.ld -T $(LINK_SCRIPT_DIR)/libs.ld -T $(LINK_SCRIPT_DIR)/sections.ld -nostartfiles -Xlinker --gc-sections -specs=nano.specs -specs=rdimon.specs -u _printf_float #-Wl,-Map,"build/linker_map.map" # -Wl,-Map,"...": linker debug output (optional)
LIB_DIRS := -L$(PULPA_LIB_DIR) -L$(SYS_LIB_DIR)
# libs.mk can define PULPA_LIBS, PULPA_IRQ_LIBS, LINK_LIBS, and change (e.g. add to) LDFLAGS
-include libs.mk
LINK_LIBS := $(PULPA_LIBS) $(LINK_LIBS)
WHOLE_LINK_LIBS := -lmkl25z $(PULPA_IRQ_LIBS)

# make dependencies
SRCS := $(wildcard src/*.c)
ASMS := $(wildcard src/*.s)
OBJS = $(subst src,$(BUILD_DIR),$(SRCS:.c=.o)) $(subst src,$(BUILD_DIR),$(ASMS:.s=.o))
HDRS := $(wildcard $(EX_INCLUDE)/*.h)
# included PULPA library files (listed in libs.mk in exercise directory)
PULPA_LIB_FILES = $(addsuffix .a,$(subst -l,lib,$(PULPA_LIBS) $(PULPA_IRQ_LIBS)))
PULPA_LIBS_WITHPATH := $(addprefix $(PULPA_LIB_DIR)/,$(PULPA_LIB_FILES))
SYS_LIB := $(SYS_LIB_DIR)/libmkl25z.a

all: $(BUILD_DIR)/app.elf
	$(info ${OBJS})

$(SYS_LIB):

$(PULPA_LIBS_WITHPATH):

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/%.o: src/%.c $(HDRS) | $(BUILD_DIR)
	$(CC) $(CFLAGS) $(INC_DIRS) -c -o $@ $<

$(BUILD_DIR)/%.o: src/%.s $(HDRS) | $(BUILD_DIR)
	$(CC) $(CFLAGS) $(INC_DIRS) -c -o $@ $<

$(BUILD_DIR)/app.elf: $(SYS_LIB) $(PULPA_LIBS_WITHPATH) $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $(LIB_DIRS) -o $@ $(OBJS) -Wl,--whole-archive $(WHOLE_LINK_LIBS) -Wl,--no-whole-archive $(LINK_LIBS)
	@echo "********** MODE = ${MODE} **********"

clean:
	rm -rf build

run:
	openocd -f board/frdm-kl25z.cfg -c init $(OPENOCD_RUN_OPTIONS) -c reset

flash: $(BUILD_DIR)/app.elf
	openocd -f board/frdm-kl25z.cfg -c "program $(BUILD_DIR)/app.elf verify exit"
	@echo "********** flashed ${MODE} code **********"

gdb:
	arm-none-eabi-gdb $(BUILD_DIR)/app.elf -ex "target remote localhost:3333" -ex "monitor reset halt" -ex "break main" -ex "continue"

.PHONY: all clean flash run gdb

