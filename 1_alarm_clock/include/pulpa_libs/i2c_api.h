/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* i2c library
************************************************************/
#ifndef I2C_API_H
#define I2C_API_H

#include "MKL25Z4.h"

enum {
  I2C_ERROR_NO_SLAVE = -1,
  I2C_ERROR_BUS_BUSY = -2
};

#define MASK(x) (1UL << (x))


void i2c_init(I2C_Type *);

// always returns 0
// internally called in read and write function
int  i2c_start(I2C_Type *);

// always returns 0
// internally called in read and write function if stop bit is set
int  i2c_stop(I2C_Type *);

// returns I2C_ERROR_NO_SLAVE = -1 or I2C_ERROR_BUS_BUSY = -2 if read failed OR
// number of bytes read
int  i2c_read(I2C_Type *, int address, char *data, int length, int stop);

// returns I2C_ERROR_NO_SLAVE = -1 or I2C_ERROR_BUS_BUSY = -2 if write failed OR
// number of bytes written
int  i2c_write(I2C_Type *, int address, const char *data, int length, int stop);

void i2c_reset(I2C_Type *);

//int i2c_byte_read(I2C_Type *, int last);

//int i2c_byte_write(I2C_Type *, int data);


#endif
