/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* display library
************************************************************/
#ifndef DISPLAY_H
#define DISPLAY_H

#include "MKL25Z4.h"

#define DISPLAY_WIDTH 											128
#define DISPLAY_HEIGHT 											64
#define DISPLAY_PIXEL_PER_BYTE							8

// I2C
#define DISPLAY_I2C_ADDRESS   							0x78

#define DISPLAY_I2C_SCL_PIN 								10 
#define DISPLAY_I2C_SCL_PIN_MODE 						2
#define DISPLAY_I2C_SCL_PT 									PTC
#define DISPLAY_I2C_SCL_PORT_MASK 					SIM_SCGC5_PORTC_MASK

#define DISPLAY_I2C_SDA_PIN 								11
#define DISPLAY_I2C_SDA_PIN_MODE 						2
#define DISPLAY_I2C_SDA_PT 									PTC
#define DISPLAY_I2C_SDA_PORT_MASK 					SIM_SCGC5_PORTC_MASK

#define DISPLAY_I2C_BUS 										I2C1

// Control byte
#define DISPLAY_CONTROL_BYTE_CMD_SINGLE    	0x80
#define DISPLAY_CONTROL_BYTE_CMD_STREAM    	0x00
#define DISPLAY_CONTROL_BYTE_DATA_SINGLE   	0xC0
#define DISPLAY_CONTROL_BYTE_DATA_STREAM   	0x40

// Fundamental commands (p.28)
#define DISPLAY_CMD_SET_CONTRAST      	    0x81    // Follow with 0x7F
#define DISPLAY_CMD_DISPLAY_RAM            	0xA4
#define DISPLAY_CMD_DISPLAY_ALLON          	0xA5
#define DISPLAY_CMD_DISPLAY_NORMAL         	0xA6
#define DISPLAY_CMD_DISPLAY_INVERTED       	0xA7
#define DISPLAY_CMD_DISPLAY_OFF            	0xAE
#define DISPLAY_CMD_DISPLAY_ON             	0xAF

// Addressing Command Table (p.30)
#define DISPLAY_CMD_SET_MEMORY_ADDR_MODE   	0x20    // Follow with 0x00 = HORZ mode = Behave like a KS108 graphic LCD
#define DISPLAY_CMD_SET_COLUMN_RANGE       	0x21    // Can be used only in HORZ/VERT mode - follow with 0x00 and 0x7F = COL127
#define DISPLAY_CMD_SET_PAGE_RANGE         	0x22    // Can be used only in HORZ/VERT mode - follow with 0x00 and 0x07 = PAGE7

// Hardware Config (p.31)
#define DISPLAY_CMD_SET_DISPLAY_START_LINE 	0x40

#define DISPLAY_CMD_SET_LOWER_COLUMN 				0x00		// 00~0F Set Lower Column Start Address for Page Addressing Mode
#define DISPLAY_CMD_SET_HIGHER_COLUMN 			0x10		// 10~1F Set Higher Column Start Address for Page Addressing Mode


#define DISPLAY_CMD_SET_SEGMENT_REMAP				0xA0 		// | 0x0 Column address 0 is mapped to seg0
																										// | 0x1 Column address 127 is mapped to seg0
#define DISPLAY_CMD_SET_MUX_RATIO          	0xA8    // Follow with 0x3F = 64 MUX
#define DISPLAY_CMD_SET_COM_SCAN_MODE      	0xC8    
#define DISPLAY_CMD_SET_DISPLAY_OFFSET     	0xD3    // Follow with 0x00
#define DISPLAY_CMD_SET_COM_PIN_MAP        	0xDA    // Follow with 0x12
#define DISPLAY_CMD_NOP                    	0xE3    // NOP

// Timing and Driving Scheme (p.32)
#define DISPLAY_CMD_SET_DISPLAY_CLK_DIV    	0xD5    // Follow with 0x80
#define DISPLAY_CMD_SET_PRECHARGE          	0xD9    // Follow with 0xF1
#define DISPLAY_CMD_SET_VCOMH_DESELCT      	0xDB    // Follow with 0x30

//
#define DISPLAY_CMD_SCAN_DIR_INC 						0xC0
#define DISPLAY_CMD_SCAN_DIR_DEC 						0xC8

// Charge Pump (pg.62)
#define DISPLAY_CMD_SET_CHARGE_PUMP        	0x8D    // Follow with 0x14


// Font 5x7 letters
#define DISPLAY_TEXT_FONT_WIDTH							5
#define DISPLAY_TEXT_FONT_HEIGHT						7
// Text
#define DISPLAY_TEXT_COLUMNS								(DISPLAY_WIDTH / (DISPLAY_TEXT_FONT_WIDTH + 1))
#define DISPLAY_TEXT_ROWS										(DISPLAY_HEIGHT / (DISPLAY_TEXT_FONT_HEIGHT + 1))

// Splash screen - full screen start image
extern unsigned const char display_splash_screen[DISPLAY_WIDTH*DISPLAY_HEIGHT/DISPLAY_PIXEL_PER_BYTE];

// Cursor directions
enum display_cursor_direction{
	// Row
	DISPLAY_CURSOR_MOVE_UP,
	DISPLAY_CURSOR_MOVE_DOWN,
	// Column
	DISPLAY_CURSOR_MOVE_LEFT,
	DISPLAY_CURSOR_MOVE_RIGHT,
	// Column and auto newline
	DISPLAY_CURSOR_MOVE_NEXT,
	// Newline
	DISPLAY_CURSOR_MOVE_NEWLINE,
};

// Image draw modes
enum display_draw_mode{
	DISPLAY_DRAW_IMAGE_NORMAL,
	DISPLAY_DRAW_IMAGE_FLIP_H,
	DISPLAY_DRAW_IMAGE_FLIP_V,
	DISPLAY_DRAW_IMAGE_FLIP_HV,
};

// Initialization
void display_enable_pins(void);
void display_init(void);

// Configuration
void display_set_brightness(unsigned char brightness);
void display_set_horizontal_mode(void);

void display_set_normal_display(void);
void display_set_inverse_display(void);

void display_set_display_on(void);
void display_set_display_off(void);


// Draw functions
// Draw a pixel
void display_draw_pixel(unsigned int x, unsigned int y);
// Draw a line
void display_draw_line(unsigned int x_start, unsigned int y_start, unsigned int x_end, unsigned int y_end);

// Draw a string at the current cursor position
void display_draw_string(const char *str);

// Draw an image at the given coordinates
void display_draw_image(const unsigned char *image, unsigned int x, unsigned int y, unsigned int width, unsigned int height, enum display_draw_mode);

// Show the buffer on the display
void display_show_buffer(void);
// Reset the buffer to 0
void display_clear_buffer(void);

// Set the cursor to (column|row)
void display_set_cursor(unsigned int row, unsigned int column);
// Move the cursor
void display_move_cursor(enum display_cursor_direction direction);

#endif
