/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* accelerometer library
************************************************************/
#ifndef MMA8451_H
#define MMA8451_H

#include "MKL25Z4.h"

#define MMA_I2C_ADDR 					0x3A
#define MMA_I2C_BUS 					I2C0

#define MMA_REG_STATUS			 	0x00
#define MMA_REG_XHI 					0x01
#define MMA_REG_XLO 					0x02
#define MMA_REG_YHI 					0x03
#define MMA_REG_YLO 					0x04
#define MMA_REG_ZHI						0x05
#define MMA_REG_ZLO 					0x06
#define MMA_REG_INT_SOURCE		0x0C
#define MMA_REG_WHOAMI	 			0x0D
#define MMA_REG_XYZ_DATA_CFG 	0x0E
#define MMA_FF_MT_CFG					0x15	// Freefall/Motion functional block configuration
#define MMA_FF_MT_SRC					0x16	// Freefall/Motion event source register
#define MMA_FF_MT_THS					0x17 	// Freefall/Motion threshold register
#define MMA_FF_MT_COUNT				0x18	// Freefall/Motion debounce counter
#define MMA_REG_CTRL1 			 	0x2A
#define MMA_REG_CTRL2 			 	0x2B
#define MMA_REG_CTRL3 			 	0x2C
#define MMA_REG_CTRL4  				0x2D
#define MMA_REG_CTRL5 			 	0x2E
#define MMA_REG_OFF_X	  			0x2F
#define MMA_REG_OFF_Y  				0x30
#define MMA_REG_OFF_Z  				0x31

#define MMA_UINT14_MAX 				0x3FFF

#define MMA_STATUS_ZYXDR 			0x08
#define MMA_WHOAMI 						0x1A

#define MMA_SENSITIVITY_2G		(4096.0) // Full scale value range 2g
#define MMA_SENSITIVITY_4G		(2048.0) // Full scale value range 4g
#define MMA_SENSITIVITY_8G		(1024.0) // Full scale value range 8g

#define MMA_COUNTS_PER_G 			MMA_SENSITIVITY_2G	
#define MMA_SHIFT_14BIT			(4.0f)
#define MMA_PI 								(3.14159265)


// 0x3D -> ODR = 1.56Hz, Reduced noise, Active mode
int mma_init(unsigned char reg_ctrl1_value);

void mma_init_pins(void);

void mma_read_full_xyz(void);

void mma_convert_xyz_to_roll_pitch(void);

// Shared variables
extern float mma_roll, mma_pitch;
extern short mma_acc_x, mma_acc_y, mma_acc_z;

extern volatile unsigned int mma_data_ready;

#endif
