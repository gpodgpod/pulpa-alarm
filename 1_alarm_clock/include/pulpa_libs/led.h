/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* LED library
************************************************************/
#ifndef __LED_H__
#define __LED_H__

enum LedColor {
  RED       = 1 << 2,
  GREEN     = 1 << 1,
  BLUE      = 1 << 0,
  PINK      = BLUE | RED,
  TURQUOISE = GREEN | BLUE,
  ORANGE    = RED | GREEN,
  WHITE     = RED | GREEN | BLUE,
  BLACK     = 0,
  OFF       = 0
};

/*
 * Sets the LED to the given color. If not yet initialized, the function will
 * set up the LEDs.
 */
void led_put_color(const enum LedColor color);

/*
 * Usually, calling this function is not required.
 * Initiates lazy initialization of LEDs
 * on the next call to led_put_color().
 */
void led_reset(void);

#endif
