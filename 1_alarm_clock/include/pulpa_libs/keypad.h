/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* keypad library
************************************************************/
#ifndef KEYPAD_H
#define KEYPAD_H

// Row pins
#define ROW1_POS (17) 		//J2 07 
#define ROW1_PORT PORTC	
#define ROW1_PT PTC

#define ROW2_POS (16)			//J2 05
#define ROW2_PORT PORTC
#define ROW2_PT PTC

#define ROW3_POS (13)			//J2 03
#define ROW3_PORT PORTC
#define ROW3_PT PTC

#define ROW4_POS (12)			//J2 01 	
#define ROW4_PORT PORTC
#define ROW4_PT PTC

// Column pins
#define COL1_POS (5)			//J1 12 
#define COL1_PORT PORTA
#define COL1_PT PTA

#define COL2_POS (4)			//J1 10
#define COL2_PORT PORTA
#define COL2_PT PTA

#define COL3_POS (12)			//J1 08
#define COL3_PORT PORTA
#define COL3_PT PTA

#define COL4_POS (2)			//J1 04
#define COL4_PORT PORTA
#define COL4_PT PTA

// Delay between setting a row signal and checking the column signals
#define KEYPAD_QUERY_DELAY 10

// Keys
enum keypad_key{
	KEYPAD_KEY_0,
	KEYPAD_KEY_1,
	KEYPAD_KEY_2,
	KEYPAD_KEY_3,
	KEYPAD_KEY_4,
	KEYPAD_KEY_5,
	KEYPAD_KEY_6,
	KEYPAD_KEY_7,
	KEYPAD_KEY_8,
	KEYPAD_KEY_9,
	KEYPAD_KEY_A,
	KEYPAD_KEY_B,
	KEYPAD_KEY_C,
	KEYPAD_KEY_D,
	KEYPAD_KEY_STAR,
	KEYPAD_KEY_HASH,
	KEYPAD_NO_KEY
};

enum keypad_key_state{
	KEYPAD_KEY_STATE_UP,
	KEYPAD_KEY_STATE_DOWN
};

// Initialize the GPIO pins and enable IRQ
void keypad_init(void);

// Get the last pressed key and clear keypress buffer
enum keypad_key keypad_read_keypress(void);

// Get the currently pressed key
enum keypad_key keypad_get_current_pressed_key(void);

#endif
