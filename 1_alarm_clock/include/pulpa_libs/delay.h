/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* delay library
************************************************************/
#ifndef DELAY_H
#define DELAY_H
extern void delay(unsigned int dlyTicks);
extern void delay10k(unsigned int dlyTicks);

#endif
