/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* adc library
************************************************************/
#ifndef ADC_H
#define ADC_H

void adc_init(void);

float adc_measure_vref(void);

#endif
