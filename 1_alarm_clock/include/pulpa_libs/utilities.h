#ifndef UTILITIES_H
#define UTILITIES_H

// Buffer has to be big enough !!!

char * itoa(int number, char * buffer, unsigned int size);

char * ftoa(float n, char * buffer, unsigned int size, unsigned int decimal_places);

#endif
