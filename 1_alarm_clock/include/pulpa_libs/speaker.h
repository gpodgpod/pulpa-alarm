/************************************************************
* Author: Uni Tuebingen, Eingebettete Systeme
* Programming Ultra Low Power Architectures
* speaker library
************************************************************/
#ifndef SPEAKER_H
#define SPEAKER_H

#define SPEAKER_MIN_FREQUENCY (0.0f)
#define SPEAKER_MAX_FREQUENCY (10000.0f)

// Initializes speaker
// Uses TPM0 Ch 4
// Connect to GND and PTC8
void speaker_init(void);
	
// Start the sound
void speaker_start(void);

// Stop the sound
void speaker_stop(void);

// Set a frequency
void speaker_set_frequency(float frequency);

// Helper function used in speaker_play()
void speaker_delay(unsigned int delay);

// Play a frequency for a specified duration
void speaker_play(float frequency, unsigned int duration);

#endif // SPEAKER_H
