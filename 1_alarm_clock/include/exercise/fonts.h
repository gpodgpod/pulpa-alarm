#ifndef FONTS_H
#define FONTS_H

// Standard ASCII font
// Font Size    : 5x8
// Memory usage : 1280 bytes
// # characters : 256
extern const unsigned char font[1280];

// (C)2010 by Henning Karlsen
// Font Size	: 8x16
// Memory usage	: 1520 bytes
// # characters	: 95
extern const unsigned char font_small[1524];

// (C)2010 by Henning Karlsen
// Font Size	: 16x16
// Memory usage	: 3040 bytes
// # characters	: 95
extern const unsigned char font_big[3044];

// Font Size    : 6x48
// Memory usage : 36 bytes
// # characters : 1
extern const unsigned char dots[36];

// Seven Segment Numbers
// Font Size	: 32x48
// Memory usage	: 1920 bytes
// # characters	: 10
extern const unsigned char font_seven_segment_numbers[1924];

// Seven Segment Numbers
// Font Size	: 29x48
// Memory usage	: 1740 bytes
// # characters	: 10
extern const unsigned char font_seven_segment_numbers2[1890];

#endif // FONTS_H
