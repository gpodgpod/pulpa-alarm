#pragma once 

#include <stdint.h>
#include "keypad.h"

typedef enum MY_VIEW_CLOCK_FORMAT {
    DIGITAL_24,
    DIGITAL_12,
    DIGITAL_ANALOG
} MY_SCREEN_CLOCK_FORMAT;

void my_view_display_config_alarm(uint32_t selected_alarm, enum keypad_key focused_item);

void my_view_display_config_alarm_list_menu(uint32_t focused_alarm);

void my_view_display_config_format_menu(enum keypad_key focused_item);

void my_view_set_format(MY_SCREEN_CLOCK_FORMAT format);

void my_view_display_config_menu(enum keypad_key focused_item);

void my_view_display_main();

void my_view_display_boot();

void my_view_display_startup();