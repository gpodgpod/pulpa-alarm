#pragma once 
#include "MKL25Z4.h"

void my_time_init(uint32_t start_time);

uint32_t my_time_get_current_time();

void my_time_increment_current_time();

void my_time_delay_second(uint32_t seconds);

typedef struct my_time_day_time {
    int hour;
    int minute;
    int seconds;
    int ampm;
} my_time_day_time;

my_time_day_time my_time_get_current_time_24h();

my_time_day_time my_time_get_current_time_12h();
