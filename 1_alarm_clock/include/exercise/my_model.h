#pragma once 

#include "my_time.h"

#define MY_MODEL_ALARM_CAPACITY 8

typedef struct MY_MODEL_ALARM{ 
    uint32_t hour;
    uint32_t minute;
    uint32_t on;
} MY_MODEL_ALARM;

void my_model_init();

void my_model_add_alarm(MY_MODEL_ALARM alarm);

MY_MODEL_ALARM * my_model_get_alarm(uint32_t alarm_pos);

void my_model_delete_alarm(uint32_t alarm_pos);

uint32_t my_model_get_alarm_count();

void my_model_increase_hour(uint32_t alarm_pos);

void my_model_decrease_hour(uint32_t alarm_pos);

void my_model_increase_minute(uint32_t alarm_pos);

void my_model_decrease_minute(uint32_t alarm_pos);

void my_model_toggle_status(uint32_t alarm_pos);