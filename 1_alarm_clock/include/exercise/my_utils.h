#pragma once 
#include <stdint.h>

void my_utils_enable_deep_sleep();

void my_utils_reset_cursor();

char my_utils_keypad_read_keypress_to_char();

void my_utils_draw_circle(uint32_t c_x, uint32_t c_y, uint32_t radius);

float my_utils_reminder(float a, float b);

void my_utils_draw_clock_line(float value, float max_value, float radius);