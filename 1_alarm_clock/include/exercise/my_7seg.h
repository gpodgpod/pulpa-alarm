#pragma once

void my_7seg_set_cursor(unsigned int new_cursor_x, unsigned int new_cursor_y);

void my_7seg_draw_string(const char *str);

void my_7seg_reset_cursor();