#pragma once 

void my_control_init_state_machine();

void my_control_start_state_machine();

float my_utils_reminder(float a, float b);

void my_utils_draw_clock_line(float value, float max_value, float radius);